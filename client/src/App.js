import "./App.css";
import { useState, useEffect } from "react";
import Axios from "axios";

function App() {
  const [password, setPassword] = useState("");
  const [title, setTitle] = useState("");
  const [passwordList, setPasswordList] = useState([]);

  useEffect(() => {
    Axios.get("http://localhost:3001/showpasswords").then((response) => {
      setPasswordList(
        response.data.map((entry) => ({ ...entry, showPassword: false }))
      );
    });
  }, []);

  const addPassword = () => {
    Axios.post("http://localhost:3001/addpassword", {
      password: password,
      title: title,
    }).then(() => {
      Axios.get("http://localhost:3001/showpasswords").then((response) => {
        setPasswordList(
          response.data.map((entry) => ({ ...entry, showPassword: false }))
        );
        alert("Password added successfully!")
      });
    })
    .catch((error) => {
      console.error("Error adding password:", error);
      alert("Failed to add password. PLease try again.")
    })
  };

  // const decryptPassword = (encryption) => {
  //   Axios.post("http://localhost:3001/decryptpassword", {
  //     password: encryption.password,
  //     iv: encryption.iv,
  //   }).then((response) => {
  //     setPasswordList(
  //       passwordList.map((val) => {
  //         return val.id === encryption.id
  //           ? {
  //               id: val.id,
  //               password: val.password,
  //               title: response.data,
  //               iv: val.iv,
  //             }
  //           : val;
  //       })
  //     );
  //   });
  // };
  const togglePasswordDisplay = (index) => {
    const entry = passwordList[index];
    if (!entry.showPassword) {
      Axios.post("http://localhost:3001/decryptpassword", {
        password: entry.password,
        iv: entry.iv,
      }).then((response) => {
        setPasswordList(
          passwordList.map((e, i) =>
            i === index
              ? {
                  ...e,
                  showPassword: !e.showPassword,
                  decryptedPassword: response.data,
                }
              : e
          )
        );
      });
    } else {
      setPasswordList(
        passwordList.map((e, i) =>
          i === index ? { ...e, showPassword: !e.showPassword } : e
        )
      );
    }
  };

  return (
    <div className="App">
      <h1 className="PasswordManagerTitle">Password Manager</h1>
      <div className="AddingPassword">
        <input
          type="text"
          placeholder="Enter Password..."
          onChange={(event) => {
            setPassword(event.target.value);
          }}
        />
        <input
          type="text"
          placeholder="Enter Title/Website..."
          onChange={(event) => {
            setTitle(event.target.value);
          }}
        />
        <button onClick={addPassword}>Add Password</button>
      </div>

      <div className="Passwords">
        <h2>Password List</h2>
        {passwordList.map((entry, index) => (
            <div
              className="password"
              onClick={() => togglePasswordDisplay(index)}
              key={index}
            >
              <h3>{entry.showPassword ? entry.decryptedPassword : entry.title}</h3>
            </div>
        ))}
      </div>
    </div>
  );
}

export default App;
